class Code
  PEGS = {"R" => :red,
          "G" => :green,
          "B" => :blue,
          "Y" => :yellow,
          "O" => :orange,
          "P" => :purple}

  def initialize(pegs)
    @pegs = pegs
  end

  attr_reader :pegs


  def self.parse(colors_str)
    chars = colors_str.chars.map(&:upcase)
    raise ArgumentError unless chars - PEGS.keys == []
    pegs = chars.map {|char| PEGS[char]}
    self.new(pegs)
  end

  def self.random
    pegs = []
    4.times {pegs << PEGS.values.sample}
    self.new(pegs)
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(other)
    matches = 0
    other.pegs.each_with_index {|color, i| matches +=1 if @pegs[i] == color}
    matches
  end

  def near_matches(other)
    matches = 0
    PEGS.values.each {|color| matches += [@pegs.count(color), other.pegs.count(color)].min}
    matches - exact_matches(other)
  end

  def ==(other)
    return false unless other.class == Code
    @pegs == other.pegs
  end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end


  def get_guess
    puts "guess the code (e.g. 'BBGY')"
    @guess = Code.parse(STDIN.gets.chomp) #@guess is an instance var of Code - object
  end

  def display_matches(code)
    puts "exact matches #{@secret_code.exact_matches(code)}"
    puts "near matches #{@secret_code.near_matches(code)}"
  end

  def play
    10.times do |turn|
      get_guess
      break if won?
      display_matches(@guess)
    end
      conclude
  end

  def won?
    @guess == @secret_code
  end

  def conclude
    if @guess == @secret_code
      puts "congratulations, you got it"
    else
      puts "sorry, you lost it this time. "
    end
    puts "The secret code was #{@secret_code.pegs}"
  end
end

if __FILE__ == $PROGRAM_NAME

  g = Game.new(Code.parse("BGYP"))
  g.play
end
